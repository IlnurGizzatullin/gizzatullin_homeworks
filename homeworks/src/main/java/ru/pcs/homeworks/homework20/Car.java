package ru.pcs.homeworks.homework20;

public class Car {

    private String licensePlateNumber;
    private String model;
    private String color;
    private int mileage;
    private int price;

    public Car(String licensePlateNumber, String model, String color, int mileage, int price) {
        this.licensePlateNumber = licensePlateNumber;
        this.model = model;
        this.color = color;
        this.mileage = mileage;
        this.price = price;
    }

    public String getLicensePlateNumber() {
        return licensePlateNumber;
    }

    public String getModel() {
        return model;
    }

    public String getColor() {
        return color;
    }

    public int getMileage() {
        return mileage;
    }

    public int getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "Car{" +
                "licensePlateNumber='" + licensePlateNumber + '\'' +
                ", model='" + model + '\'' +
                ", color='" + color + '\'' +
                ", mileage=" + mileage +
                ", price=" + price +
                '}';
    }
}
