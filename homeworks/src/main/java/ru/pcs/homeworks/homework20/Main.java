package ru.pcs.homeworks.homework20;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/*
    Подготовить файл с записями, имеющими следующую структуру:
    [НОМЕР_АВТОМОБИЛЯ][МОДЕЛЬ][ЦВЕТ][ПРОБЕГ][СТОИМОСТЬ]
    Используя Java Stream API, вывести:
    Номера всех автомобилей, имеющих черный цвет или нулевой пробег. // filter + map
    Количество уникальных моделей в ценовом диапазоне от 700 до 800 тыс. // distinct + filter
    * Вывести цвет автомобиля с минимальной стоимостью. // min + map
    * Среднюю стоимость Camry
    https://habr.com/ru/company/luxoft/blog/270383/
    Достаточно сделать один из вариантов.
*/
public class Main {

    public static void main(String[] args) {
        List<Car> cars = getAllCars("hw20Input.txt");
//        List<Car> cars = Collections.emptyList();
        getPlateNumberByColorOrMileage(cars, "Black", 0)
                .forEach(System.out::println);

        System.out.println(getUniqueModelCountInPriceRange(cars, 700000, 800000));

        System.out.println(getColorOfMinPriceCar(cars));

        System.out.println(getAveragePriceByModel(cars, "Camry"));
    }

    public static List<String> getPlateNumberByColorOrMileage(List<Car> cars, String color, int mileage) {
        return cars.stream()
                .filter(car -> car.getColor().equals(color) || car.getMileage() == mileage)
                .map(Car::getLicensePlateNumber)
                .collect(Collectors.toList());
    }

    public static long getUniqueModelCountInPriceRange(List<Car> cars, int min, int max) {
        return cars.stream()
                .filter(car -> car.getPrice() >= min && car.getPrice() <= max)
                .map(Car::getModel)
                .distinct()
                .count();
    }

    public static String getColorOfMinPriceCar(List<Car> cars) {
        return cars.stream()
                .min(Comparator.comparing(Car::getPrice))
                .map(Car::getColor)
                .orElse(null);
    }

    public static double getAveragePriceByModel(List<Car> cars, String carModel) {
        return cars.stream()
                .filter(car -> car.getModel().equals(carModel))
                .mapToDouble(Car::getPrice)
                .average()
                .orElse(0.0);
    }

    private static List<Car> getAllCars(String filename) {
        List<Car> cars = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(filename))) {
            String line = reader.readLine();
            while (line != null) {
                String[] parts = line.split("\\|");
                String licensePlateNumber = parts[0];
                String model = parts[1];
                String color = parts[2];
                int mileage = Integer.parseInt(parts[3]);
                int price = Integer.parseInt(parts[4]);
                Car car = new Car(licensePlateNumber, model, color, mileage, price);
                cars.add(car);
                line = reader.readLine();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        return cars;
    }
}
