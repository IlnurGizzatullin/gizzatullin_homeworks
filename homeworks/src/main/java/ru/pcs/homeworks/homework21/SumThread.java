package ru.pcs.homeworks.homework21;

public class SumThread extends Thread {
    private int from;
    private int to;
    private int threadId;

    public SumThread(int from, int to, int threadId) {
        this.from = from;
        this.to = to;
        this.threadId = threadId;
    }

    @Override
    public void run() {
        for (int i = from; i <= to; i++) {
            Main.sums[threadId] += Main.array[i];
        }
    }
}

