package ru.pcs.homeworks.homework06;

import java.util.Arrays;

public class Homework06 {
    /*   1. Реализовать функцию, принимающую на вход массив и целое число. Данная функция должна вернуть индекс этого числа в массиве.
          Если число в массиве отсутствует - вернуть -1.
         2. Реализовать процедуру, которая переместит все значимые элементы влево, заполнив нулевые, например:
            было:
                34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20
            стало
                34, 14, 15, 18, 1, 20, 0, 0, 0, 0, 0
    */

    public static int getIndexOfNumber(int[] array, int number) {
        int index = -1;
        for (int i = 0; i < array.length; i++) {
            if (number == array[i]) {
                index = i;
                break;
            }
        }
        return index;
    }

    public static void replaceNonZeroNumbersToLeft(int[] array) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == 0) {
                for (int j = i; j < array.length; j++) {
                    if (array[j] != 0) {
                        array[i] = array[j];
                        array[j] = 0;
                        break;
                    }
                }
            }
        }
    }

    public static void main(String[] args) {
        int[] array = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 5};
        System.out.println(getIndexOfNumber(array, 5));
        System.out.println(getIndexOfNumber(array, 10));

        int[] initialArray = {34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20};
        replaceNonZeroNumbersToLeft(initialArray);
        System.out.println(Arrays.toString(initialArray));
    }
}
