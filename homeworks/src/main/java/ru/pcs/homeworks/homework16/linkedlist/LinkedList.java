package ru.pcs.homeworks.homework16.linkedlist;

public class LinkedList<T> {

    private static class Node<T> {
        T value;
        Node<T> next;

        public Node(T value) {
            this.value = value;
        }
    }

    private Node<T> first;
    private Node<T> last;
    private int size;

    public void add(T element) {
        // создаю новый узел
        Node<T> newNode = new Node<>(element);
        if (size == 0) {
            first = newNode;
        } else {
            last.next = newNode;
        }
        last = newNode;
        size++;
    }

    public void addToBegin(T element) {
        Node<T> newNode = new Node<>(element);

        if (size == 0) {
            last = newNode;
        } else {
            newNode.next = first;
        }
        first = newNode;
        size++;
    }

    private boolean isCorrectIndex(int index) {
        return index >= 0 && index < size;
    }

    public int size() {
        return size;
    }

    /**
     * Получить элемент по индексу
     *
     * @param index индекс искомого элемента
     * @return элемент под заданным индексом
     */
    public T get(int index) {
        if (isCorrectIndex(index)) {
            int count = 0;
            Node<T> node = first;
            while (count < index) {
                node = node.next;
                count++;
            }
            return node.value;
        }
        return null;
    }
}

