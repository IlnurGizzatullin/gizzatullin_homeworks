package ru.pcs.homeworks.homework16.arraylist;

public class Main {

    public static void main(String[] args) {
        ArrayList<Integer> numbers = new ArrayList<>();
        numbers.add(33);
        numbers.add(15);
        numbers.add(11);
        numbers.add(89);
        numbers.add(17);
        numbers.add(21);
        numbers.add(89);
        numbers.add(13);
        numbers.add(29);
        numbers.add(56);
        numbers.add(19);

        System.out.println("size = " + numbers.size());
        for (int i = 0; i < numbers.size(); i++) {
            System.out.print(numbers.get(i) + " ");
        }
        System.out.println();
        numbers.removeAt(0);
        System.out.println("size = " + numbers.size());
        for (int i = 0; i < numbers.size(); i++) {
            System.out.print(numbers.get(i) + " ");
        }
        System.out.println();
        numbers.removeAt(4);
        System.out.println("size = " + numbers.size());
        for (int i = 0; i < numbers.size(); i++) {
            System.out.print(numbers.get(i) + " ");
        }
    }
}

