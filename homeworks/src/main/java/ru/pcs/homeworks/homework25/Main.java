package ru.pcs.homeworks.homework25;

import org.springframework.jdbc.datasource.DriverManagerDataSource;
import ru.pcs.homeworks.homework25.repository.ProductRepository;
import ru.pcs.homeworks.homework25.repository.ProductRepositoryJdbcTemplateImpl;

import javax.sql.DataSource;

public class Main {
    public static void main(String[] args) {
        DataSource dataSource = new DriverManagerDataSource("jdbc:postgresql://localhost:5432/",
                "postgres", "postgres");

        ProductRepository productRepository = new ProductRepositoryJdbcTemplateImpl(dataSource);
        productRepository.findAll().forEach(System.out::println);
        System.out.println();

        productRepository.findAllByPrice(4500).forEach(System.out::println);
        System.out.println();

        productRepository.findAllByOrdersCount(2).forEach(System.out::println);
    }
}