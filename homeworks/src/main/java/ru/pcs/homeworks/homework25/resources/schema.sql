DROP TABLE IF EXISTS orders;
DROP TABLE IF EXISTS product;
DROP TABLE IF EXISTS customer;

CREATE TABLE product
(
    id          SERIAL PRIMARY KEY,
    description VARCHAR                      NOT NULL,
    price       DECIMAL                      NOT NULL,
    amount       INTEGER CHECK ( amount >= 0 ) NOT NULL
);

CREATE TABLE customer
(
    id         SERIAL PRIMARY KEY,
    first_name VARCHAR NOT NULL,
    last_name  VARCHAR NOT NULL
);

CREATE TABLE orders
(
    product_id    INTEGER                              NOT NULL,
    customer_id   INTEGER                              NOT NULL,
    date_time     TIMESTAMP                            NOT NULL,
    product_count INTEGER CHECK ( product_count >= 0 ) NOT NULL,
    FOREIGN KEY (product_id) REFERENCES product (id),
    FOREIGN KEY (customer_id) REFERENCES customer (id)
);

INSERT INTO product (description, price, amount)
VALUES ('Блендер', 1990, 10),
       ('Кофемашина', 2300, 4),
       ('Ланч-бокс', 2500, 15),
       ('Мультиварка', 6800, 3),
       ('Мясорубка', 4500, 6),
       ('Миксер', 2200, 15),
       ('Тостер', 2700, 7);

INSERT INTO customer (first_name, last_name)
VALUES ('Максим', 'Максимов'),
       ('Игорь', 'Петров'),
       ('Иван', 'Иванов');

INSERT INTO orders (product_id, customer_id, date_time, product_count)
VALUES (2, 1, '2021-11-20 10:00:00', 2),
       (2, 1, '2021-11-21 17:00:00', 1),
       (6, 1, '2021-11-27 17:00:00', 4),
       (1, 2, '2021-11-23 12:00:00', 3),
       (6, 2, '2021-11-27 12:00:00', 3),
       (2, 2, '2021-11-25 10:00:00', 1),
       (3, 3, '2021-11-26 10:00:00', 4),
       (5, 3, '2021-11-27 10:00:00', 1),
       (5, 3, '2021-11-28 10:00:00', 2);


-- вывести имя и фамилию человека, у которого в заказах есть Кофемашина
SELECT DISTINCT c.first_name, c.last_name
FROM orders o
         JOIN customer c ON c.id = o.customer_id
         JOIN product p ON p.id = o.product_id
WHERE p.description = 'Кофемашина';

-- вывести название товара и количество товара, заказанного '2021-11-27'
SELECT p.description, SUM(o.product_count) AS product_count
FROM product p
         LEFT JOIN orders o ON p.id = o.product_id
WHERE DATE(date_time) = '2021-11-27'
GROUP BY p.description;

--вывести имя, фамилию человека и количество его заказов
SELECT c.first_name, c.last_name, COUNT(o) AS orders_count
FROM orders o
         JOIN customer c ON o.customer_id = c.id
GROUP BY c.first_name, c.last_name;

--вывести наибольшую стоимость заказа и дату заказа
SELECT (p.price * o.product_count) AS order_price, DATE(o.date_time)
FROM product p
         JOIN orders o on p.id = o.product_id
ORDER BY order_price DESC
LIMIT 1;