package ru.pcs.homeworks.homework25.repository;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import ru.pcs.homeworks.homework25.model.Product;

import javax.sql.DataSource;
import java.util.List;

public class ProductRepositoryJdbcTemplateImpl implements ProductRepository {

    private JdbcTemplate jdbcTemplate;

    public ProductRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    private static final RowMapper<Product> ROW_MAPPER = (rs, rowNum) -> Product.builder()
            .id(rs.getInt("id"))
            .description(rs.getString("description"))
            .price(rs.getDouble("price"))
            .amount(rs.getInt("amount"))
            .build();

    @Override
    public List<Product> findAll() {
        return jdbcTemplate.query("SELECT * FROM product ORDER BY id", ROW_MAPPER);
    }

    @Override
    public List<Product> findAllByPrice(double price) {
        return jdbcTemplate.query("SELECT * FROM product WHERE price=?", ROW_MAPPER, price);
    }

    @Override
    public List<Product> findAllByOrdersCount(int ordersCount) {
        return jdbcTemplate.query("""
                SELECT p.id, p.description, p.price, p.amount FROM product p INNER JOIN orders o ON p.id = o.product_id
                GROUP BY p.id HAVING COUNT(o.product_id) = ?;
                """, ROW_MAPPER, ordersCount);
    }
}