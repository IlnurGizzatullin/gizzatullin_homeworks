package ru.pcs.homeworks.homework25.repository;

import ru.pcs.homeworks.homework25.model.Product;

import java.util.List;

public interface ProductRepository {

    List<Product> findAll();

    List<Product> findAllByPrice(double price);

    List<Product> findAllByOrdersCount(int ordersCount);
}