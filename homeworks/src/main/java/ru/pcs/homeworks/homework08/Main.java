package ru.pcs.homeworks.homework08;

import java.util.Scanner;

public class Main {

    /*
    На вход подается информация о людях в количестве 10 человек (имя - строка, вес - вещественное число).
    Считать эти данные в массив объектов.
    Вывести в отсортированном по возрастанию веса порядке.
   */

    public static Human[] fillHumanArray(int length) {
        Human[] humans = new Human[length];
        Scanner scanner = new Scanner(System.in);

        for (int i = 0; i < humans.length; i++) {
            humans[i] = new Human();
            humans[i].setName(scanner.nextLine());
            humans[i].setWeight(Double.parseDouble(scanner.nextLine()));
        }
        scanner.close();
        return humans;
    }

    public static void quickSort(Human[] humans, int left, int right) {
        if (humans.length == 0) {
            return;
        }
        if (left >= right) {
            return;
        }
        int pivot = (left + right) / 2;
        Human pivotHuman = humans[pivot];
        int i = left;
        int j = right;
        while (i <= j) {
            while (humans[i].getWeight() < pivotHuman.getWeight()) {
                i++;
            }
            while (humans[j].getWeight() > pivotHuman.getWeight()) {
                j--;
            }
            if (i <= j) {
                Human temp = humans[i];
                humans[i] = humans[j];
                humans[j] = temp;
                i++;
                j--;
            }
        }
        if (left < j) {
            quickSort(humans, left, j);
        }
        if (right > i) {
            quickSort(humans, i, right);
        }
    }

    public static void sortHumansByWeight(Human[] humans) {
        for (int i = 0; i < humans.length; i++) {
            for (int j = i + 1; j < humans.length; j++) {
                if (humans[i].getWeight() > humans[j].getWeight()) {
                    Human temp = humans[i];
                    humans[i] = humans[j];
                    humans[j] = temp;
                }
            }
        }
    }

    public static void main(String[] args) {
        /*    Тестовые данные:
        first
        50.6
        second
        90.7
        third
        70.6
        fourth
        56.9
        fifth
        81.3
        sixth
        48.8
        seventh
        77.7
        eighth
        75.5
        ninth
        100.0
        tenth
        66.4
        */
        Human[] humans = fillHumanArray(10);
        quickSort(humans, 0, humans.length - 1);
        for (Human human : humans) {
            System.out.println(human);
        }
    }
}
