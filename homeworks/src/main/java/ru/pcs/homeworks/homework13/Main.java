package ru.pcs.homeworks.homework13;

import java.util.Arrays;

public class Main {
    /*
    Предусмотреть функциональный интерфейс
    interface ByCondition {
	    boolean isOk(int number);
    }
    Реализовать в классе Sequence метод:
    public static int[] filter(int[] array, ByCondition condition) {
	    ...
	}
    Данный метод возвращает массив, который содержит элементы, удовлетворяющие логическому выражению в condition.
    В main в качестве condition подставить:
    - проверку на четность элемента
    - проверку, является ли сумма цифр элемента четным числом.
    */
    public static void main(String[] args) {
        int[] array = {23, 11, 33, 44, 24, 100, 52, 17, 65, 22};
        int[] evenElements = Sequence.filter(array, element -> element % 2 == 0);
        System.out.println(Arrays.toString(evenElements));

        int[] evenElementDigitsSum = Sequence.filter(array, element -> {
            int sum = 0;
            while (element != 0) {
                sum += element % 10;
                element = element / 10;
            }
            return sum % 2 == 0;
        });
        System.out.println(Arrays.toString(evenElementDigitsSum));
    }
}
