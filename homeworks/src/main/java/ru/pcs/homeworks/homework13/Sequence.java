package ru.pcs.homeworks.homework13;

import java.util.Arrays;

public class Sequence {

    public static int[] filter(int[] array, ByCondition condition) {
        int count = 0;
        int[] temp = new int[array.length];
        for (int element : array) {
            if (condition.isOk(element)) {
                temp[count] = element;
                count++;
            }
        }
        return Arrays.copyOf(temp, count);
    }
}
