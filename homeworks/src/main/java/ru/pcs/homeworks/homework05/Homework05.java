package ru.pcs.homeworks.homework05;

import java.util.Scanner;

public class Homework05 {
    /*Реализовать программу на Java, которая для последовательности чисел, оканчивающихся
     на -1 выведет самую минимальную цифру, встречающуюся среди чисел последовательности.
     Например: 345 298 456 -1
     Ответ: 2
     */
    public static void main(String[] args) {
        int minDigit = 9;
        Scanner scanner = new Scanner(System.in);
        int currentNumber = scanner.nextInt();

        while (currentNumber != -1) {
            currentNumber = Math.abs(currentNumber);
            while (currentNumber != 0) {
                int digit = currentNumber % 10;
                if (digit < minDigit) {
                    minDigit = digit;
                }
                currentNumber = currentNumber / 10;
            }
            currentNumber = scanner.nextInt();
        }
        System.out.println(minDigit);
    }
}