package ru.pcs.homeworks.attestation01_oop.repository;

import ru.pcs.homeworks.attestation01_oop.User;

import java.util.List;

public interface UsersRepository {

    User findById(int id);

    void update(User user);

    List<User> findAll();

}

