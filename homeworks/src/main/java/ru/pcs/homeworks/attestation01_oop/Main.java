package ru.pcs.homeworks.attestation01_oop;

import ru.pcs.homeworks.attestation01_oop.repository.UsersRepository;
import ru.pcs.homeworks.attestation01_oop.repository.UsersRepositoryFileImpl;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        UsersRepository usersRepository = new UsersRepositoryFileImpl("src/attestation01_oop/users.txt");

        List<User> usersBefore = usersRepository.findAll();
        for (User user : usersBefore) {
            System.out.println(user.getId() + " " + user.getName() + " " + user.getAge() + " " + user.isWorker());
        }

        User userForUpdate = usersRepository.findById(2);
        userForUpdate.setName("Mars");
        userForUpdate.setAge(27);
        usersRepository.update(userForUpdate);

        System.out.println("------------------------------------------------------------");

        List<User> usersAfter = usersRepository.findAll();
        for (User user : usersAfter) {
            System.out.println(user.getId() + " " + user.getName() + " " + user.getAge() + " " + user.isWorker());
        }
    }
}
