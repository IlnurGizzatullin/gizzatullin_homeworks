package ru.pcs.homeworks.attestation01_oop;

public class NotFoundException extends RuntimeException {

    public NotFoundException(String message) {
        super(message);
    }
}
