package ru.pcs.homeworks.attestation01_oop.repository;

import ru.pcs.homeworks.attestation01_oop.NotFoundException;
import ru.pcs.homeworks.attestation01_oop.User;

import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class UsersRepositoryFileImpl implements UsersRepository {

    private final String fileName;
    private final Map<Integer, User> users = new HashMap<>();

    public UsersRepositoryFileImpl(String fileName) {
        this.fileName = fileName;
        findAll().forEach(user -> users.put(user.getId(), user));
    }

    /**
     * Retrieves user by its id
     *
     * @param id unique user id
     * @return the user with the given id
     * @throws NotFoundException if repository does not contain user with the given id
     */
    @Override
    public User findById(int id) {
        Optional<User> user = Optional.ofNullable(users.get(id));
        return user.orElseThrow(() -> new NotFoundException("Not found User with id " + id));
    }

    /**
     * Update user and overwrite users file with updated user record
     *
     * @param user user for update
     */
    @Override
    public void update(User user) {
        if (user == null) {
            return;
        }
        users.put(user.getId(), user);
        clearFile(fileName);
        saveAll();
    }

    /**
     * Erases the contents of the file
     *
     * @param fileName the name of the file to clean up
     */
    private void clearFile(String fileName) {
        try {
            new FileWriter(fileName, false).close();
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    /**
     * Saves all user records to file
     */
    private void saveAll() {
        try (Writer writer = new FileWriter(fileName, true);
             BufferedWriter bufferedWriter = new BufferedWriter(writer)) {
            for (User u : users.values()) {
                bufferedWriter.write(String.format("%s|%s|%s|%s", u.getId(), u.getName(), u.getAge(), u.isWorker()));
                bufferedWriter.newLine();
            }
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    /**
     * Returns all user records from file
     *
     * @return List of users
     */
    @Override
    public List<User> findAll() {
        List<User> users;
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))) {
            users = bufferedReader.lines()
                    .map(line -> {
                        String[] parts = line.split("\\|");
                        int id = Integer.parseInt(parts[0]);
                        String name = parts[1];
                        int age = Integer.parseInt(parts[2]);
                        boolean isWorker = Boolean.parseBoolean(parts[3]);
                        return new User(id, name, age, isWorker);
                    }).toList();
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
        return users;
    }
}
