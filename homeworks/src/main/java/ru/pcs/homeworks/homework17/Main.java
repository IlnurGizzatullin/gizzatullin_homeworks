package ru.pcs.homeworks.homework17;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/*
    На вход подается строка с текстом. Необходимо посчитать, сколько встречается раз каждое слово в этой строке.
    Вывести:
    Слово - количество раз
    Использовать Map, string.split(" ") - для деления текста по словам. Слово - символы, ограниченные пробелами справа и слева.

    Строка для проверки: one two three four five two three four five one, four Five three
    */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String line = scanner.nextLine();
        scanner.close();

        String[] words = line.split(" ");

        Map<String, Integer> wordsCount = getWordsCount(words);
        wordsCount.entrySet().forEach(System.out::println);
    }

    public static Map<String, Integer> getWordsCount(String[] words) {
        Map<String, Integer> wordsCount = new HashMap<>();
        for (String word : words) {
            if (!word.isBlank()) {
                wordsCount.merge(word, 1, Integer::sum);
            }
        }
        return wordsCount;
    }
}
