package ru.pcs.homeworks.homework19;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        UsersRepository usersRepository = new UsersRepositoryFileImpl("users.txt");
        List<User> users = usersRepository.findAll();

        for (User user : users) {
            System.out.println(user.getAge() + " " + user.getName() + " " + user.isWorker());
        }

        User user = new User("Игорь", 33, true);
        usersRepository.save(user);

        List<User> usersByAge = usersRepository.findByAge(33);
        usersByAge.forEach(System.out::println);

        System.out.println();

        List<User> usersIsWorkers = usersRepository.findByIsWorkerIsTrue();
        usersIsWorkers.forEach(System.out::println);

    }
}

