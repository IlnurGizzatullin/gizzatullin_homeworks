package ru.pcs.homeworks.homework19;

import java.util.List;

public interface UsersRepository {
    List<User> findAll();

    void save(User user);

    List<User> findByAge(int age);

    List<User> findByIsWorkerIsTrue();
}

