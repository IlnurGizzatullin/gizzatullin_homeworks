package ru.pcs.homeworks.homework19;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class UsersRepositoryFileImpl implements UsersRepository {

    private String fileName;

    public UsersRepositoryFileImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public List<User> findByAge(int age) {
        return findAllByPredicate(user -> user.getAge() == age);
    }

    @Override
    public List<User> findByIsWorkerIsTrue() {
        return findAllByPredicate(User::isWorker);
    }

    @Override
    public List<User> findAll() {
        return findAllByPredicate(user -> true);
    }

    private List<User> findAllByPredicate(Predicate<User> filter) {
        List<User> users = new ArrayList<>();
        try (Reader reader = new FileReader(fileName);
             BufferedReader bufferedReader = new BufferedReader(reader)) {
            String line = bufferedReader.readLine();
            while (line != null) {
                String[] parts = line.split("\\|");
                String name = parts[0];
                int age = Integer.parseInt(parts[1]);
                boolean isWorker = Boolean.parseBoolean(parts[2]);
                User newUser = new User(name, age, isWorker);
                if (filter.test(newUser)) {
                    users.add(newUser);
                }
                line = bufferedReader.readLine();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        return users;
    }

    @Override
    public void save(User user) {
        try (Writer writer = new FileWriter(fileName, true);
             BufferedWriter bufferedWriter = new BufferedWriter(writer)) {

            bufferedWriter.write(user.getName() + "|" + user.getAge() + "|" + user.isWorker());
            bufferedWriter.newLine();
            bufferedWriter.flush();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
