package ru.pcs.homeworks.homework07;

import java.util.Scanner;

public class Homework07 {

    /*
    На вход подается последовательность чисел, оканчивающихся на -1
    Необходимо вывести число, которе присутствует в последовательности минимальное количество раз.
    Гарантируется:
    Все числа в диапазоне от -100 до 100.
    Числа встречаются не более 2 147 483 647-раз каждое.
    Сложность алгоритма - O(n)
    */
    public static void main(String[] args) {
        int[] numberCount = new int[201];
        Scanner scanner = new Scanner(System.in);
        int input = scanner.nextInt();

        while (input != -1) {
            numberCount[input + 100] += 1;
            input = scanner.nextInt();
        }
        scanner.close();

        int count = Integer.MAX_VALUE;
        int index = 0;
        for (int i = 0; i < numberCount.length; i++) {
            if (i == 99) continue;
            if (count > numberCount[i]) {
                count = numberCount[i];
                index = i;
            }
        }
        int requiredNumber = index - 100;
        System.out.println("первое число, которе присутствует в последовательности минимальное количество раз " +
                "в диапазоне от -100 до 100 = " + requiredNumber);
    }
}
