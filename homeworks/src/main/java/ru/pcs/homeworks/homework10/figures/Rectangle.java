package ru.pcs.homeworks.homework10.figures;

public class Rectangle extends Figure {

    private int sideA;
    private int sideB;

    public Rectangle(int x, int y, int sideA, int sideB) {
        super(x, y);
        this.sideA = sideA;
        this.sideB = sideB;
    }

    public double getPerimeter() {
        return (sideA + sideB) * 2;
    }
}