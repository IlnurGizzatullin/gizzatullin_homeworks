package ru.pcs.homeworks.homework10;


import ru.pcs.homeworks.homework10.figures.Circle;
import ru.pcs.homeworks.homework10.figures.Square;

import static ru.pcs.homeworks.homework10.Movable.DEFAULT_X;
import static ru.pcs.homeworks.homework10.Movable.DEFAULT_Y;

public class Main {
    /*
    Сделать класс Figure из задания 09 абстрактным.
    Определить интерфейс, который позволит перемещать фигуру на заданные координаты.
    Данный интерфейс должны реализовать только классы Circle и Square.
    В Main создать массив "перемещаемых" фигур и сдвинуть все их в одну конкретную точку.
    */
    public static void main(String[] args) {
        Circle circle = new Circle(5, 5, 10);
        Square square = new Square(2, 6, 8);

        Movable[] figures = new Movable[2];
        figures[0] = circle;
        figures[1] = square;

        for (Movable figure : figures) {
            System.out.println("Before move: " + figure);
            figure.move(DEFAULT_X, DEFAULT_Y);
            System.out.println("After move: " + figure);
        }
    }
}