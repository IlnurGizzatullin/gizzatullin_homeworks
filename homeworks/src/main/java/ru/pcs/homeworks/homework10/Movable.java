package ru.pcs.homeworks.homework10;

public interface Movable {
    int DEFAULT_X = 0;
    int DEFAULT_Y = 0;

    void move(int x, int y);
}
