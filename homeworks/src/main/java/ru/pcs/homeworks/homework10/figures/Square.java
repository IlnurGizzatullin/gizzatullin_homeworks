package ru.pcs.homeworks.homework10.figures;

import ru.pcs.homeworks.homework10.Movable;

public class Square extends Rectangle implements Movable {

    public Square(int x, int y, int side) {
        super(x, y, side, side);
    }

    @Override
    public void move(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        return "Square{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}