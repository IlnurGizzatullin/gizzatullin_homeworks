package ru.pcs.homeworks.homework10.figures;

import ru.pcs.homeworks.homework10.Movable;

public class Circle extends Ellipse implements Movable {

    public Circle(int x, int y, int radius) {
        super(x, y, radius, radius);
    }

    @Override
    public void move(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        return "Circle{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}