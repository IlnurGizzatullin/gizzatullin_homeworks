package ru.pcs.homeworks.homework10.figures;

public class Ellipse extends Figure {

    private int radius1;
    private int radius2;

    public Ellipse(int x, int y, int radius1, int radius2) {
        super(x, y);
        this.radius1 = radius1;
        this.radius2 = radius2;
    }

    public double getPerimeter() {
        return 4 * (Math.PI * radius1 * radius2 + (radius1 - radius2)) / (radius1 + radius2);
    }
}