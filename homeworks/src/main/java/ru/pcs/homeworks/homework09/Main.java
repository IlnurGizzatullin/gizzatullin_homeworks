package ru.pcs.homeworks.homework09;

import ru.pcs.homeworks.homework09.figures.*;

public class Main {
    /*
    Сделать класс Figure, у данного класса есть два поля - x и y координаты.
    Классы Ellipse и Rectangle должны быть потомками класса Figure.
    Класс Square - потомок класса Rectangle, Circle - потомок класса Ellipse.
    В классе Figure предусмотреть метод getPerimeter(), который возвращает 0. Во всех остальных классах он должен возвращать корректное значение.
    */
    public static void main(String[] args) {
        Figure ellipse = new Ellipse(0, 0, 10, 5);
        Figure circle = new Circle(0, 0, 5);
        Figure rectangle = new Rectangle(0, 0, 10, 5);
        Figure square = new Square(0, 0, 5);

        System.out.println(ellipse.getPerimeter());
        System.out.println(circle.getPerimeter());
        System.out.println(rectangle.getPerimeter());
        System.out.println(square.getPerimeter());
    }
}