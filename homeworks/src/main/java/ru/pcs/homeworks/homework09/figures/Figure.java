package ru.pcs.homeworks.homework09.figures;

public class Figure {

    private int x;
    private int y;

    public Figure(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public double getPerimeter() {
        return 0.0;
    }
}