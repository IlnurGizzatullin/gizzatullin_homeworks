package ru.pcs.homeworks.homework09.figures;

public class Square extends Rectangle {

    public Square(int x, int y, int side) {
        super(x, y, side, side);
    }
}