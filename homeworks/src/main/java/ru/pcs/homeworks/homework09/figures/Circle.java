package ru.pcs.homeworks.homework09.figures;

public class Circle extends Ellipse {

    public Circle(int x, int y, int radius) {
        super(x, y, radius, radius);
    }
}