package ru.pcs.homeworks.homework26;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName(value = "NumbersUtil is working when")
@DisplayNameGeneration(value = DisplayNameGenerator.ReplaceUnderscores.class)
class NumbersUtilTest {

    private final NumbersUtil numbersUtil = new NumbersUtil();

    @Nested
    @DisplayName("gcd() is working")
    public class ForGcd {

        @ParameterizedTest(name = "gcd {2} for {0} and {1}")
        @CsvSource(value = {"18, 12, 6", "9, 12, 3", "64, 48, 16", "1, 12, 1", "9, 9, 9", "26, 100, 2"})
        public void return_correct_gcd(int a, int b, int result) {
            assertEquals(result, numbersUtil.gcd(a, b));
        }

        @ParameterizedTest(name = "throws IllegalArgumentException on {0} and {1}")
        @CsvSource(value = {"0, 0", "0, 12", "64, 0", "-50, 6", "6, -3", "-64, -20", "-7, 0", "0, -12",})
        public void bad_numbers_throws_exception(int a, int b) {
            assertThrows(IllegalArgumentException.class, () -> numbersUtil.gcd(a, b));
        }
    }
}